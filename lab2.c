#include "mbed.h"
 
/*Serial pc(USBTX, USBRX); // tx, rx
PwmOut led(LED1);
Serial uart(p28, p27);*/ 


DigitalOut coil_a(p24);
DigitalOut coil_b(p23);
DigitalOut coil_c(p22);
DigitalOut coil_d(p21);

float brightness = 0.0;
 
int main() {
   // pc.printf("Echoes back to the screen anything you type\n");
    // pc.printf("Press 'u' to turn LED1 brightness up, 'd' to turn it down\n");
    float wait_time = 0.0017; 
    coil_a = 0; 
    coil_b = 0; 
    coil_c = 0; 
    coil_d = 0; 
    
    while(1) {
        coil_d = 1;        
        wait(wait_time);      
        coil_a = 1; 
        wait(wait_time);     
        coil_d = 0;
        wait(wait_time);           
        coil_b = 1; 
        wait(wait_time);   
        coil_a = 0;
        wait(wait_time);    
        coil_c = 1;
        wait(wait_time);   
        coil_b = 0;
        wait(wait_time);     
        coil_c = 0;
    }
}